#!/bin/bash
# Call ./sameStep.sh | wc -l


cryslengths=($(seq 1 0.01 2.5))
energies=($(seq 0 2.5 120))

outPath="/eos/project/i/insulab-como/datiVari/202301_simulPMG_PbWO4"


for i in ${cryslengths[@]}
do
	for j in ${energies[@]}
	do
		echo "Lunghezza cristallo: ${i} - Energia $j"
	done
done
