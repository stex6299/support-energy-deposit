# What's here

This repository contains few scripts in support of [PMG simulation scan](https://gitlab.com/piemmegi/energy-loss)


# Scripts
- [verificaFileMancante.sh](verificaFileMancante.sh) Permette di verificare e printare quali file relativi allo scan non sono presenti nella cartella di destinazione. I cicli sono copiati dal `mastercall.sh` della simulazione del PMG
- [updateWeb.sh](updateWeb.sh) Permette di aggiornare un [quaderno jupyter](integraleSimPietro.ipynb) e di servirlo come una pagina web. Può essere fatto girare in un tmux come `k5reauth ./updateWeb.sh`
- [sameStep.sh](sameStep.sh) Esempio per il PMG di come si potrebbe fare lo stesso ciclo, ma usando direttamente i valori dei passi di scan invece che gli indici


# Altre info utili
Per quanto riguarda la cartella `Outputfiles`, è stato creato un link simbolico ad EOS `ln -s /eos/project/i/insulab-como/datiVari/202301_simulPMG_PbWO4 Outputfiles`
