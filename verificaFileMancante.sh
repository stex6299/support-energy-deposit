cryslengths=($(seq 1 0.01 2.5))
energies=($(seq 0 2.5 120))

outPath="/eos/project/i/insulab-como/datiVari/202301_simulPMG_PbWO4"


nvalues_energies=${#energies[@]}
nvalues_lengths=${#cryslengths[@]}



for ((i = 0 ; i < nvalues_lengths ; i++)); do
	for ((j = 0 ; j < nvalues_energies ; j++)); do
		#echo "Lunghezza cristallo: ${i} - Energia $j"
		# Nome file: lossdata_length_150_energy_039.dat
		ii=$(printf "%03d" $i)
		jj=$(printf "%03d" $j)

		if [ ! -f $outPath/lossdata_length_${ii}_energy_${jj}.dat ]; then
			echo "$outPath/lossdata_length_${ii}_energy_${jj}.dat --  does not exists"
		fi
	done
done
